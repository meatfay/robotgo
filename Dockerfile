FROM php:7.4.2-cli

RUN apt-get update && apt-get install vim -y && \
    apt-get install openssl -y && \
    apt-get install libssl-dev -y && \
    apt-get install wget -y && \
    apt-get install git -y && \
    apt-get install procps -y && \
    apt-get install htop -y

RUN cd /tmp && git clone https://github.com/swoole/swoole-src.git && \
    cd swoole-src && \
    git checkout v4.4.16 && \
    phpize  && \
    ./configure  --enable-openssl && \
    make -j 4 && make install

RUN cd /tmp && \
    git clone https://github.com/swoole/async-ext.git && cd async-ext && \
    git checkout tags/v4.4.16 && \
    ./make.sh && \
    docker-php-ext-enable swoole_async

RUN touch /usr/local/etc/php/conf.d/swoole.ini && \
    echo 'extension=swoole.so\nextension=swoole_async.so' >> /usr/local/etc/php/conf.d/swoole.ini

RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64
RUN chmod +x /usr/local/bin/dumb-init

RUN apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/local/bin/dumb-init", "--", "php"]