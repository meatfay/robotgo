#Installation
После клонирования:
```shell script
composer install
```

Затем:
```shell script
docker run --rm -p 9501:9501 --name=robogo -v $(pwd):/app -w /app swoole-php server.php
```