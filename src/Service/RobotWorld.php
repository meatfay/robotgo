<?php

namespace App\Service;

use App\Entity\PathInfo;
use App\Exception\OutOfWallsException;

class RobotWorld
{
    const MAX_X_WIDTH = 100;
    const MIN_X_WIDTH = 0;

    const MAX_Y_WIDTH = 100;
    const MIN_Y_WIDTH = 0;

    /**
     * @var int
     */
    private int $currentY = 0;

    /**
     * @var int
     */
    private int $currentX = 0;

    /**
     * Store info about path
     * @var PathInfo
     */
    private PathInfo $pathInfo;

    /**
     * @var boolean
     */
    private bool $hasWalls;

    /**
     * RobotWorld constructor.
     * @param PathInfo $pathInfo
     * @param bool $hasWalls
     */
    public function __construct(PathInfo $pathInfo, $hasWalls = false)
    {
        $this->pathInfo = $pathInfo;
        $this->hasWalls = $hasWalls;

        $this->currentX = intval($pathInfo->getCurrentPosition()[0]);
        $this->currentY = intval($pathInfo->getCurrentPosition()[1]);
    }

    /**
     * @param integer $step
     * @throws OutOfWallsException
     */
    public function stepY($step)
    {
        if (!$this->hasWalls && ($this->currentY + $step < self::MIN_Y_WIDTH || $this->currentY + $step > self::MAX_Y_WIDTH)) {
            throw new OutOfWallsException($this->getCurrentPosition());
        } elseif ($this->hasWalls && ($this->currentY + $step <= self::MIN_Y_WIDTH || $this->currentY + $step >= self::MAX_Y_WIDTH)) {
            $this->currentY += -($step);
            $this->writePathLog();
            return;
        }

        $this->currentY += $step;
        $this->writePathLog();
    }

    /**
     * @param integer $step
     * @throws OutOfWallsException
     */
    public function stepX($step)
    {
        if (!$this->hasWalls && ($this->currentX + $step < self::MIN_X_WIDTH || $this->currentX + $step > self::MAX_X_WIDTH)) {
            throw new OutOfWallsException($this->getCurrentPosition());
        } elseif ($this->hasWalls && ($this->currentX + $step <= self::MIN_X_WIDTH || $this->currentX + $step >= self::MAX_X_WIDTH)) {
            $this->currentX += -($step);
            $this->writePathLog();
            return;
        }

        $this->currentX += $step;
        $this->writePathLog();
    }

    /**
     * @return array
     */
    public function getCurrentPosition()
    {
        return [$this->currentX, $this->currentY];
    }

    /**
     * Set new position to pathInfo
     */
    private function writePathLog()
    {
        $this->pathInfo->setCurrentPosition($this->getCurrentPosition());
        $this->pathInfo->addPathToLog($this->getCurrentPosition());
    }
}