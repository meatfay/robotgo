<?php

namespace App\Service;

use App\Entity\PathInfo;
use App\Exception\OutOfWallsException;

class RobotWalk
{
    /**
     * @param array $getParams
     * @return PathInfo|null
     * @throws OutOfWallsException
     */
    public function run(array $getParams): ?PathInfo
    {
        $command = $getParams['command'];

        $commands = explode(';', $command);

        if (!$commands) {
            return null;
        }

        $pathStore = new PathStore();

        $hasWalls = $getParams['has_walls'] === 'true';

        $pathInfo = $this->generatePathInfo($pathStore, $getParams['start_position']);

        $robotWorld = new RobotWorld($pathInfo, $hasWalls);

        $robotPath = new RobotPath($robotWorld, $commands);

        if (!$robotPath->validatePath()) {
            return null;
        }

        $robotPath->move();

        $pathStore->savePathLog($pathInfo);

        return $pathInfo;
    }

    /**
     * Generate PathInfo object by given params
     *
     * @param PathStore $pathStore
     * @param $startPosition
     * @return PathInfo
     */
    private function generatePathInfo(PathStore $pathStore, $startPosition)
    {
        $pathInfo = new PathInfo();

        $pathInfo->setCurrentPosition([1, 1]);
        $pathInfo->addPathToLog([1, 1]);

        $parsedPosition = explode(';', $startPosition);

        if (count($parsedPosition) === 2) {
            $pathStore->cleanPathLog();

            $pathInfo->setPathLog([]);
            $pathInfo->setCurrentPosition([
                intval($parsedPosition[0]),
                intval($parsedPosition[1]),
            ]);
            $pathInfo->addPathToLog($pathInfo->getCurrentPosition());
        } elseif ($pathStore->isPathLogFileExists()) {
            $pathInfo->setPathLog([]);

            $pathStore->readPathLog($pathInfo);
        }

        return $pathInfo;
    }
}