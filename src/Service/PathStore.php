<?php

namespace App\Service;

use App\Entity\PathInfo;
use Swoole\Async;

class PathStore
{
    /**
     * @var string
     */
    private string $logFile;

    public function __construct()
    {
        $this->logFile = dirname(__FILE__) . '/../../logs/path.log';
    }

    /**
     * Check is log file exists
     * @return bool
     */
    public function isPathLogFileExists()
    {
        return file_exists($this->logFile);
    }

    /**
     * Read saved log
     * @param PathInfo $pathInfo
     */
    public function readPathLog(PathInfo $pathInfo)
    {
        $file = fopen($this->logFile, "r");

        if ($file) {
            while (($line = fgets($file))) {
                $pathLog = array_map(fn($item) => array_map('intval', explode(',', $item)), explode(';', $line));

                $pathInfo->setPathLog(
                    $pathLog
                );

                $pathInfo->setCurrentPosition($pathLog[count($pathLog) - 1]);
            }

            fclose($file);
        }
    }

    /**
     * Clean log file
     */
    public function cleanPathLog()
    {
        $file = fopen($this->logFile, "r+");

        if ($file) {
            ftruncate($file, 0);
            fclose($file);
        }
    }

    /**
     * Save path to log file
     * @param PathInfo $pathLog
     */
    public function savePathLog(PathInfo $pathLog): void
    {
        $log = implode(";", array_map(fn($item) => implode(",", $item), $pathLog->getPathLog()));

        Async::write($this->logFile, $log, 0);
    }
}