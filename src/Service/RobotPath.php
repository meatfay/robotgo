<?php

namespace App\Service;

use App\Exception\OutOfWallsException;

class RobotPath
{
    const STEP = 1;

    private array $directionsMap = [
        'up' => self::STEP,
        'down' => -self::STEP,
        'right' => self::STEP,
        'left' => -self::STEP,
    ];

    private array $directionName = [
        'x' => ['left', 'right'],
        'y' => ['up', 'down'],
    ];

    /**
     * Command list
     * @var array
     */
    private array $path = [];

    /**
     * @var RobotWorld
     */
    private RobotWorld $robotWorld;

    /**
     * RobotPath constructor.
     * @param $robotWorld
     * @param array $path
     */
    public function __construct(RobotWorld $robotWorld, $path = [])
    {
        $this->robotWorld = $robotWorld;
        $this->path = $path;
    }

    /**
     * @throws OutOfWallsException
     */
    public function move(): void
    {
        foreach ($this->path as $step) {
            if (in_array($step, $this->directionName['y'])) {
                $this->robotWorld->stepY($this->directionsMap[$step]);
            }

            if (in_array($step, $this->directionName['x'])) {
                $this->robotWorld->stepX($this->directionsMap[$step]);
            }
        }
    }

    /**
     * Check is all commands is valid
     * @return bool
     */
    public function validatePath(): bool
    {
        $stepNames = array_keys($this->directionsMap);

        foreach ($this->path as $step) {
            if (!in_array($step, $stepNames)) {
                return false;
            }
        }

        return true;
    }
}