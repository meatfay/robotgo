<?php

namespace App\Exception;

class OutOfWallsException extends \Exception
{
    public function __construct($position)
    {
        $message = sprintf("Вы вышли за пределы карты!\nПозиция: X: %d Y: %d\n", $position[0], $position[1]);
        parent::__construct($message);
    }
}