<?php

namespace App\Entity;

class PathInfo
{
    /**
     * @var array
     */
    private array $currentPosition = [];

    /**
     * @var array
     */
    private array $pathLog = [];

    /**
     * @return array
     */
    public function getCurrentPosition(): array
    {
        return $this->currentPosition;
    }

    /**
     * @param array $currentPosition
     */
    public function setCurrentPosition(array $currentPosition)
    {
        $this->currentPosition = $currentPosition;
    }

    /**
     * @return array
     */
    public function getPathLog(): array
    {
        return $this->pathLog;
    }

    /**
     * @param array $pathLog
     */
    public function setPathLog(array $pathLog)
    {
        $this->pathLog = $pathLog;
    }

    public function addPathToLog(array $path)
    {
        $this->pathLog[] = $path;
    }
}