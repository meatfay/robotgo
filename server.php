<?php

use App\Entity\PathInfo;
use App\Exception\OutOfWallsException;
use App\Service\RobotWalk;
use Swoole\Http\Request;
use Swoole\Http\Response;

/** @var RobotWalk $robotWalk */
$robotWalk = require 'bootstrap.php';

$http = new Swoole\HTTP\Server("0.0.0.0", 9501);

$http->on('start', function ($server) {
    echo "Swoole http server is started at http://127.0.0.1:9501\n";
});

$http->on('request', function (Request $request, Response $response) use($robotWalk) {
    $response->header("Content-Type", "text/plain; charset=utf-8");

    if (empty($request->get)) {
        $response->end("Нет ни одной команды!\nДоступные команды: left, right, up, down\n");
        return;
    }

    try {
        /** @var PathInfo $pathInfo */
        $pathInfo = $robotWalk->run($request->get);
    } catch (OutOfWallsException $ex) {
        $response->end($ex->getMessage());
        return;
    }

    if (is_null($pathInfo)) {
        $response->end("Ошибка в команде. Перепроверьте правильность введенной команды!\nДоступные команды: left, right, up, down\n");
        return;
    }

    $response->end(sprintf("X: %d\nY: %d\n", $pathInfo->getCurrentPosition()[0], $pathInfo->getCurrentPosition()[1]));
});

$http->start();